# Handover

v0.1.0

```
Endings are better than beginnings.
Sticking to it is better than standing out.

- Ecclesiastes 7:8
```

## Passing control and responsibility

It is time. For other things, or just a break. So let us help the ones that pick up where we left off. To make that happen we need to provide our successors with the information needed be able to proceed as smoothly and swiftly as possible. So how do we go about doing just that?

#### Scope

There are (at least) three ways find out what needs to be handed over; What are you responsible for? What are the projects you work on? What do your successors need to know? This not a solo effort, you need feedback from your team lead and or colleague(s).

 * Responsibility
 * Projects
 * Information handover
 * Hardware handover

#### 5WH

With our list of responsibilities and projects we will need to get into the details of the Handover. 5WH is a good tool for that. 5WH stands for:

 * What
	 - Project name
	 - Deadlines
 * Why
 * Who
	 - From
	 - To
 * Where
 * When
	 - Starting Handover
	 - Leaving position
 * How
	 - Prequisites
	 - Access
	
	
## Checklists
 * [Android Project Handover](Android/README.md)

 
## Licence (MIT)

Copyright 2019, Kornelis Marijs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
