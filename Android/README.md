# Android Project Handover Checklist

With a change of programmer for a project there is a list of things needed to be checked, installed and put in place.

This document is written as a Playbook on how to hand over an Android Project. 
The goal is to enable the new programmer to build the latest version of the project and have access to the Google Play Store to update if nessesary.

## Release Requirements
 * [ ] Release keys
 * [ ] Release keystore
 * [ ] Android Play Store Access
	
## Development Requirements
 * Location Access
	* [ ] Office
	* [ ] Network
	* [ ] Wifi 
 * Software used
	* [ ] Android Studio
	* [ ] Git bash
	* [ ] Etc (DOCUMENT what is beeing used!)
 * Project files
	* [ ] Git
 * Debug & Test hardware
	* [ ] Devices
	* [ ] Services
 * Tests
	* [ ] Automated App tests
	* [ ] UI tests
	* [ ] API Tests
	* [ ] User tests
	* [ ] Programmer tests
 
## Various
 * [ ] Privacy Policy
 * [ ] API end point